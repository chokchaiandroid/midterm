import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class MidtermA17 {

    public static void main(String[] args) {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
        String inputStr = null;

        List<Integer> dataList = new ArrayList<Integer>();

        try {
            while ((inputStr = bufReader.readLine()) != null) {
                // System.out.println(inputStr.trim());
                int numb = Integer.parseInt(inputStr.trim());
                dataList.add(numb);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // for(Integer data:dataList){
        // System.out.println(data);
        // }

        long start = System.currentTimeMillis();

        int c = 300;
        int num = 0;

        for (int i = 0; i < dataList.size(); i++) {
            for (int j = i + 1; j < dataList.size(); j++) {
                if (j != i && (dataList.get(i) + dataList.get(j) == c))
                    // System.out.println(array[i] + "+" + array[j]);
                    num++;
            }
        }
        System.out.println("Number in Array A that sum = c : " + (num));
        long end = System.currentTimeMillis();

        NumberFormat formatter = new DecimalFormat("#0.00000000000000");
        System.out.print("Execution time is " + formatter.format((end - start) / 1000d) + " seconds");
    }

}
