import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;

class MidtermA19 {
    public static void main(String[] args) {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in));
        String inputStr = null;

        String result = "";

        long start = System.nanoTime();
        try {
            while ((inputStr = bufReader.readLine()) != null) {

                result += inputStr;  
            }
            // System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        int numb = Integer.parseInt(result);
        System.out.println("String to integer is :" + numb);

        long end = System.nanoTime();

       NumberFormat formatter = new DecimalFormat("#0");
        System.out.print("Execution time is " + formatter.format((end - start)) + " nanotime");
    }

}
